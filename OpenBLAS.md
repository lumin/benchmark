OpenBLAS
===

### Matrix Multiplication with Julia

#### Result chart

Trim results with this command line: `cat result | grep -v julia | grep -v OPENBLAS | grep -v mean | tr -d '|\n' | sed -e 's/  / /g'`.  

![chart](benchmark.gemm.chart.png)  

![chart2](benchmark.gemm.chart.6900k.png)  

Enclosed area: the smaller the better.

#### Code

`a.jl`
```julia
# configurations
iterations = 20
matsize = 2000

# dump configurations
# println("matsize = $(matsize); $((matsize^2 * 8)/1024)KiB")

# benchmark functions
function benchmark_gemm(matsize, iterations)
	α = 1.0
    A = rand(matsize,matsize) # Float64
    B = rand(matsize,matsize)
	β = 0.0
	C = zeros(matsize, matsize)

	# GEMM: αAB+βC -> C
	t = Float64[]
	for i in 1:iterations
		push!(t, @elapsed BLAS.gemm!('N', 'N', α, A, B, β, C))
	end
	return t
end

# helper function
function benchmark_report(t)
	@printf("| minimum  | maximum  |   mean   |  median  |\n")
	@printf("| %7f | %7f | %7f | %7f |\n",
		 minimum(t), maximum(t), mean(t), median(t))
end

# benchmark report
benchmark_report(benchmark_gemm(matsize, iterations))
```

`a.sh`
```shell
#!/bin/sh
set -e -x

OPENBLAS_NUM_THREADS=1 julia a.jl
OPENBLAS_NUM_THREADS=2 julia a.jl
OPENBLAS_NUM_THREADS=4 julia a.jl

OPENBLAS_NUM_THREADS=1 OPENBLAS_CORETYPE=Unknown julia a.jl
OPENBLAS_NUM_THREADS=2 OPENBLAS_CORETYPE=Unknown julia a.jl
OPENBLAS_NUM_THREADS=4 OPENBLAS_CORETYPE=Unknown julia a.jl
```

#### Results on Debian/Sid, I5-2520M, 2Core 4Threads @ 3.2GHz, L3=3072K

###### Case1: official openblas, official julia

```
libopenblas-base/unstable,now 0.2.19-2 amd64 [installed]
julia/unstable,now 0.4.7-6 amd64 [installed]


+ OPENBLAS_NUM_THREADS=1

| minimum  | maximum  |   mean   |  median  |
| 0.686916 | 0.779289 | 0.701670 | 0.693540 |

+ OPENBLAS_NUM_THREADS=2

| minimum  | maximum  |   mean   |  median  |
| 0.428918 | 0.445400 | 0.435307 | 0.434597 |

+ OPENBLAS_NUM_THREADS=4

| minimum  | maximum  |   mean   |  median  |
| 0.710930 | 0.766887 | 0.724836 | 0.723401 |

+ OPENBLAS_NUM_THREADS=1 + OPENBLAS_CORETYPE=Unknown

| minimum  | maximum  |   mean   |  median  |
| 1.696451 | 1.926305 | 1.809083 | 1.808741 |

+ OPENBLAS_NUM_THREADS=2 + OPENBLAS_CORETYPE=Unknown

| minimum  | maximum  |   mean   |  median  |
| 0.973859 | 1.479665 | 1.109295 | 1.077079 |

+ OPENBLAS_NUM_THREADS=4 + OPENBLAS_CORETYPE=Unknown

| minimum  | maximum  |   mean   |  median  |
| 1.017403 | 1.466177 | 1.205780 | 1.204780 |
```

###### Case2: -march=native openblas, official julia

```
libopenblas-base/now 0.2.19-2native1 amd64 [installed,local]
julia/unstable,now 0.4.7-6 amd64 [installed]


+ OPENBLAS_NUM_THREADS=1

| minimum  | maximum  |   mean   |  median  |
| 0.695317 | 0.814535 | 0.751549 | 0.750175 |

+ OPENBLAS_NUM_THREADS=2

| minimum  | maximum  |   mean   |  median  |
| 0.415140 | 0.686166 | 0.508630 | 0.496420 |

+ OPENBLAS_NUM_THREADS=4

| minimum  | maximum  |   mean   |  median  |
| 0.721619 | 0.855509 | 0.778891 | 0.773818 |

+ OPENBLAS_NUM_THREADS=1 + OPENBLAS_CORETYPE=Unknown

| minimum  | maximum  |   mean   |  median  |
| 1.701005 | 1.831953 | 1.731713 | 1.724158 |

+ OPENBLAS_NUM_THREADS=2 + OPENBLAS_CORETYPE=Unknown

| minimum  | maximum  |   mean   |  median  |
| 0.933155 | 0.974739 | 0.947907 | 0.946319 |

+ OPENBLAS_NUM_THREADS=4 + OPENBLAS_CORETYPE=Unknown

| minimum  | maximum  |   mean   |  median  |
| 0.926095 | 1.469042 | 1.125609 | 1.080820 |
```

#### Results on Debian/Sid, I7-6900k, 8Core 16Threads @ 4.0GHz, L3=20480k

###### Case1: official openblas, official julia

```
+ OPENBLAS_NUM_THREADS=1
+ julia a.jl
| minimum  | maximum  |   mean   |  median  |
| 0.431532 | 0.468027 | 0.433651 | 0.431814 |
+ OPENBLAS_NUM_THREADS=2
+ julia a.jl
| minimum  | maximum  |   mean   |  median  |
| 0.219741 | 0.317928 | 0.226852 | 0.220555 |
+ OPENBLAS_NUM_THREADS=4
+ julia a.jl
| minimum  | maximum  |   mean   |  median  |
| 0.112403 | 0.308156 | 0.166892 | 0.125209 |
+ OPENBLAS_NUM_THREADS=8
+ julia a.jl
| minimum  | maximum  |   mean   |  median  |
| 0.060291 | 0.122106 | 0.068333 | 0.065259 |
+ OPENBLAS_NUM_THREADS=16
+ julia a.jl
| minimum  | maximum  |   mean   |  median  |
| 0.060973 | 0.123373 | 0.070471 | 0.065955 |
+ OPENBLAS_NUM_THREADS=1
+ OPENBLAS_CORETYPE=Unknown
+ julia a.jl
| minimum  | maximum  |   mean   |  median  |
| 1.340679 | 1.511935 | 1.367763 | 1.357123 |
+ OPENBLAS_NUM_THREADS=2
+ OPENBLAS_CORETYPE=Unknown
+ julia a.jl
| minimum  | maximum  |   mean   |  median  |
| 0.676990 | 0.864558 | 0.711307 | 0.714084 |
+ OPENBLAS_NUM_THREADS=4
+ OPENBLAS_CORETYPE=Unknown
+ julia a.jl
| minimum  | maximum  |   mean   |  median  |
| 0.356897 | 0.468476 | 0.365670 | 0.357994 |
+ OPENBLAS_NUM_THREADS=8
+ OPENBLAS_CORETYPE=Unknown
+ julia a.jl
| minimum  | maximum  |   mean   |  median  |
| 0.181748 | 0.470171 | 0.218171 | 0.182571 |
+ OPENBLAS_NUM_THREADS=16
+ OPENBLAS_CORETYPE=Unknown
+ julia a.jl
| minimum  | maximum  |   mean   |  median  |
| 0.181489 | 0.278713 | 0.191479 | 0.182122 |
```

###### Case2: -march=native openblas, official julia

```
+ OPENBLAS_NUM_THREADS=1
+ julia a.jl
| minimum  | maximum  |   mean   |  median  |
| 0.431767 | 0.476101 | 0.434219 | 0.431916 |
+ OPENBLAS_NUM_THREADS=2
+ julia a.jl
| minimum  | maximum  |   mean   |  median  |
| 0.219610 | 0.315863 | 0.227352 | 0.219860 |
+ OPENBLAS_NUM_THREADS=4
+ julia a.jl
| minimum  | maximum  |   mean   |  median  |
| 0.111805 | 0.199042 | 0.120021 | 0.111886 |
+ OPENBLAS_NUM_THREADS=8
+ julia a.jl
| minimum  | maximum  |   mean   |  median  |
| 0.060158 | 0.123035 | 0.068414 | 0.065158 |
+ OPENBLAS_NUM_THREADS=16
+ julia a.jl
| minimum  | maximum  |   mean   |  median  |
| 0.060788 | 0.122957 | 0.070027 | 0.065843 |
+ OPENBLAS_NUM_THREADS=1
+ OPENBLAS_CORETYPE=Unknown
+ julia a.jl
| minimum  | maximum  |   mean   |  median  |
| 1.345954 | 1.413291 | 1.353566 | 1.348734 |
+ OPENBLAS_NUM_THREADS=2
+ OPENBLAS_CORETYPE=Unknown
+ julia a.jl
| minimum  | maximum  |   mean   |  median  |
| 0.674806 | 0.857124 | 0.700472 | 0.697331 |
+ OPENBLAS_NUM_THREADS=4
+ OPENBLAS_CORETYPE=Unknown
+ julia a.jl
| minimum  | maximum  |   mean   |  median  |
| 0.357422 | 0.482703 | 0.368401 | 0.358280 |
+ OPENBLAS_NUM_THREADS=8
+ OPENBLAS_CORETYPE=Unknown
+ julia a.jl
| minimum  | maximum  |   mean   |  median  |
| 0.181584 | 0.283125 | 0.190512 | 0.182741 |
+ OPENBLAS_NUM_THREADS=16
+ OPENBLAS_CORETYPE=Unknown
+ julia a.jl
| minimum  | maximum  |   mean   |  median  |
| 0.182322 | 0.277847 | 0.194262 | 0.182518 |
```

# debdiff

```patch
$ debdiff openblas_0.2.19-2.dsc openblas_0.2.19-2native1.dsc
dpkg-source: warning: extracting unsigned source package (/home/lumin/packages/openblas.pkg/openblas_0.2.19-2native1.dsc)
diff -Nru openblas-0.2.19/debian/changelog openblas-0.2.19/debian/changelog
--- openblas-0.2.19/debian/changelog	2017-01-23 14:06:15.000000000 +0000
+++ openblas-0.2.19/debian/changelog	2017-02-24 10:34:33.000000000 +0000
@@ -1,3 +1,9 @@
+openblas (0.2.19-2native1) UNRELEASED; urgency=medium
+
+  * -march=native build.
+
+ -- lumin <lumin@Sid>  Fri, 24 Feb 2017 10:34:33 +0000
+
 openblas (0.2.19-2) unstable; urgency=medium
 
   * Also build libopenblas-dev on mips64el. (Closes: #852283)
diff -Nru openblas-0.2.19/debian/rules openblas-0.2.19/debian/rules
--- openblas-0.2.19/debian/rules	2017-01-23 14:06:15.000000000 +0000
+++ openblas-0.2.19/debian/rules	2017-02-24 10:34:33.000000000 +0000
@@ -3,6 +3,9 @@
 include /usr/share/dpkg/buildflags.mk
 include /usr/share/dpkg/pkg-info.mk
 
+export DEB_CFLAGS_MAINT_APPEND = -march=native
+export DEB_FFLAGS_MAINT_APPEND = -march=native
+
 MAKE_OPTIONS := NO_LAPACKE=1 NO_AFFINITY=1 NO_WARMUP=1 CFLAGS="$(CPPFLAGS) $(CFLAGS)" FFLAGS="$(FFLAGS)"
 
 ifneq (,$(findstring noopt,$(DEB_BUILD_OPTIONS)))
```

# Reference

[1] https://github.com/xianyi/OpenBLAS/wiki/faq  
[2] src:OpenBLAS driver/others/dynamic.c  
[3] src:Julia base/linalg/blas.jl  

# License

```
Copyright © 2017 Lumin <cdluminate@qq.com>
License: BSD-2-Clause, see LICENSE
```