dSFMT
===

## Result chart

![chart](dsfmt-19937-2520m.png)  
![chart2](dsfmt-19937-6900k.png)  

Enclosed area: the smaller the better.

Case1: official binary

Case2: -march=native rebuild

## Code

`a.c`
```c++
#include <stdio.h>
#include <stdlib.h>
#define DSFMT_MEXP 19937
#include <dSFMT.h>

int
main(int argc, char *argv[])
{
	dsfmt_t dsfmt;
	long i;
	double x;

	dsfmt_init_gen_rand(&dsfmt, 12345);
	for (i = 0; i < 999999999; i++)
		x = dsfmt_genrand_close_open(&dsfmt);

	return 0;
}
```

`b.sh`
```shell
#!/bin/sh
set -e

gcc -O2 a.c -o a -ldSFMT-19937

for i in $(seq 20); do
	/usr/bin/time -p ./a 2>&1 | grep user | awk '{print $2}'
done
```

#### Results on Debian/Sid, I5-2520M

###### Case1: official dsfmt (with SSE2 support)

```
t = [
1.49
1.64
1.58
1.52
1.52
1.52
1.51
1.55
1.52
1.50
1.49
1.50
1.52
1.50
1.54
1.59
1.60
1.54
1.53
1.47
]
println(minimum(t))
println(maximum(t))
println(mean(t))
println(median(t))

---

1.47
1.64
1.5314999999999999
1.52
```

###### Case2: -march=native dsfmt (`export DEB_CFLAGS_MAINT_APPEND=-march=native`)

```
t = [
1.46
1.42
1.44
1.45
1.38
1.41
1.37
1.39
1.38
1.40
1.37
1.38
1.37
1.37
1.38
1.38
1.38
1.37
1.49
1.45
]
println(minimum(t))
println(maximum(t))
println(mean(t))
println(median(t))

---

1.37
1.49
1.4019999999999997
1.38
```

#### Results on Debian/Sid, I7-6900K

###### Case1: official dsfmt

```
t = [
1.19
1.19
1.17
1.19
1.20
1.19
1.19
1.19
1.19
1.19
1.08
1.09
1.19
1.20
1.18
1.17
1.19
1.04
1.19
1.20
]
println(minimum(t))
println(maximum(t))
println(mean(t))
println(median(t))

1.04
1.2
1.1709999999999998
1.19
```

###### Case2: -march=native dsfmt

```
t = [
1.12
1.12
1.12
1.22
1.12
1.12
1.12
1.02
1.02
1.12
1.12
1.12
1.12
1.12
1.12
0.91
1.12
1.12
0.92
1.12
]
println(minimum(t))
println(maximum(t))
println(mean(t))
println(median(t))

0.91
1.22
1.0945000000000005
1.12
```