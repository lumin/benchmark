#!/bin/sh
# Automated OpenBLAS Benchmarking program for gitlab.com/lumin/benchmark
# Copyright (C) 2017 Lumin
# MIT License
set -e

# FIXME: this is still a work-in-progress

# usage:
# 1. compile openblas with generic flags, run the tests
#     $ bash crunch.sh
# 2. compile openblas with native flags, run the tests
#     $ MYPROFILE=native bash crunch.sh
# 3. collect the graphs and see if there is anything fun
#     $ SKIP=1 DO_PLOT=1 bash crunch.sh

# configuration
iterations=100             # the number of test iterations
profile="generic"          # profile \in { generic, native }
switch_do_plot=0           # should we plot the graph
switch_skip=0              # should we skip data collection

# provide the way to override configurations with ENV
if ! test -z "$ITER"; then iterations=$ITER; fi
if ! test -z "$MYPROFILE"; then profile=$MYPROFILE; fi
if ! test -z "$DO_PLOT"; then switch_do_plot=$DO_PLOT; fi
if ! test -z "$SKIP"; then switch_skip=$SKIP; fi

# scan all the test programs
TESTS=$(ls *.goto)
echo I: there are $(ls -1 *.goto | wc -l) test binaries available.

# helper functions : data processing
function processor_xGEMM () {
  # $ nl dgemm.goto.log.raw 
  # 1	From :   1  To : 200 Step=1 : Trans=N
  # 2	   SIZE          Flops          Time
  # 3	      1x1 :        0.14 MFlops   0.000014 sec
  # 4	      2x2 :       16.00 MFlops   0.000001 sec
  # 5	      3x3 :       54.00 MFlops   0.000001 sec
  # 6	      4x4 :      128.00 MFlops   0.000001 sec
  # .................. 202 lines in total
  if test -z "$1"; then echo E: are you kidding me?; exit 1; fi
  if test -z "$2"; then echo E: are you kidding me?; exit 1; fi
  if ! test -r $1; then
    echo E: file $1 not readable.; exit 1
  fi
  tail -n200 $1 | awk '{print $3}' > $2
}
function processor () {
  # this is a dispatcher
  if test -z "$1"; then echo E: are you kidding me?; exit 1; fi
  if echo "$1" | grep gemm 1>/dev/null; then
    processor_xGEMM $1 $1.tidy
  # else
    #cp $1 $1.tidy # we'd better not to do so
  fi
}

# helper functions: plotting black magic with Octave
function plot_xGEMM () {
  local debug
  debug=1
  # glob data files
  DATA_GENERIC=$(ls -1 $1.log.generic.raw.*.tidy)
  DATA_NATIVE=$(ls -1 $1.log.native.raw.*.tidy)
  count_generic=$(ls -1 $1.log.generic.raw.*.tidy | wc -l)
  count_native=$(ls -1 $1.log.native.raw.*.tidy | wc -l)
  script=junk.m

  # generate octave script : header
  truncate -s 0 $script
  echo "%!/usr/bin/octave" >> $script
  echo "% automatically generated script" >> $script
  echo "" >> $script

  # generate octave script : loading data
  for item in $DATA_GENERIC $DATA_NATIVE; do
    sane_varname=$(echo $item | sed -e 's/\./_/g')
	echo "$sane_varname = load('$item');" >> $script
  done
  printf "total_generic = 0 " >> $script
  for item in $DATA_GENERIC; do
    sane_varname=$(echo $item | sed -e 's/\./_/g')
    printf "+ $sane_varname " >> $script
  done
  printf ";\n" >> $script
  printf "total_native = 0 " >> $script
  for item in $DATA_NATIVE; do
    sane_varname=$(echo $item | sed -e 's/\./_/g')
    printf "+ $sane_varname " >> $script
  done
  printf ";\n" >> $script
  echo "total_generic = total_generic / $count_generic;" >> $script
  echo "total_native  = total_native  / $count_native;" >> $script

  # generate octave script : plot
  template_octave_script="
  figure;\n\
  plot(1:length(total_generic), total_generic, total_native);\n\
  title('$1 : Generic vs. Native');\n\
  xlabel('matrix size');\n\
  ylabel('MFlops');\n\
  legend('generic', 'native');\n\
  grid on;\n\
  print -dpng $1.png;\n\
  "
  echo -e $template_octave_script >> $script

  # optionally dump the generated file
  if test debug = 1; then cat $script; fi
  # run octave
  octave -q $script
}
function plot () {
  # this is a dispatcher
  if test -z "$1"; then echo E: I need data!; exit 1; fi
  if echo "$1" | grep gemm; then
    # first check data
    for pro in generic native; do
      for i in $(seq $iterations); do
        if ! test -r $1.log.$pro.raw.$i.tidy; then
          echo E: no sufficient data found: $1.log.$pro.raw.$i.tidy; exit 1
        fi
      done
    done
    echo I: sketching curve for $1 ...
    plot_xGEMM $1
  # else
  #   do nothing
  fi
}

# the traversal for data collection
if test 0 = $switch_skip; then
  for needle in $TESTS; do
    echo I: collecting data from $needle ...
    # create log file in 'w+' mode, and put the program output into it
    for i in $(seq $iterations); do
      truncate -s 0 $needle.log.$profile.raw.$i
      ./$needle >> $needle.log.$profile.raw.$i 2>&1
      processor $needle.log.$profile.raw.$i
    done
  done
fi

# the traversal for drawing graphs
if ! test 0 = $switch_do_plot; then
  for needle in $TESTS; do
    plot $needle
  done
fi

echo I: done.
