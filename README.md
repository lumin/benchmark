Benchmarking (Work-in-progress)
===

This repo is pre-study for a [GSoC project](https://wiki.debian.org/SummerOfCode2017/Projects/Benchmarking).  

The GSoC project application encountered trouble (2017/04/20), but this repo will still be kept and maintained.

"Scientific program benchmarking" is too large a topic, and we need to do some investigations before
deciding what to benchmark in the GSoC project mentioned above.

Currently we are working on some general purpose scientific computing libraries
as listed below:

* [OpenBLAS](OpenBLAS.md) -- BLAS essential library for linear algebra  
* [GMP]  
* [MPFR]  
* [FFTW] -- Fast Fourier Transformation   
* [dSFMT](dSFMT.md) -- MT19937 pseudo random number generator  
* to be added  


# Credits

Thanks to @de_licious for [Julia performance tips](https://gitlab.com/lumin/benchmark/issues/1)
and [library suggestion](https://gitlab.com/lumin/benchmark/issues/2) and many other very
helpful guidance and suggestions.  

# License

```
Copyright © 2017 Lumin <cdluminate@qq.com>
License: BSD-2-Clause, see LICENSE
```